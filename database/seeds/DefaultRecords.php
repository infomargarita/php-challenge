<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Entries;
use Illuminate\Support\Facades\Hash;


class DefaultRecords extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
          ['name' => 'Test user 1',
           'email' => 'test1@domain.com',
           'password' => Hash::make('testpassword1'),
           'twitter_handle' => 'linode'
           ]);

        User::create(
            ['name' => 'Test user 2',
             'email' => 'test2@domain.com',
             'password' => Hash::make('testpassword2'),
             'twitter_handle' => 'digitalocean'
             ]);



    }
}
