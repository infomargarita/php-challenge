
## **Jobsity PHP challenge by José Romero**
sistemasjg at gmail.com

This application is developed using Laravel 6. 
Due to twitter dev API need to apply for access, I used some old keys for old testing porpouses

## **Requires**

 - php 7.x
 - composer
 - npm
 

## **How to Install**

First, you need to clone this repository into your local machine 

    cd /var/www/jobsitychallenge/

    git clone https://sistemasjg@bitbucket.org/infomargarita/php-challenge.git jose_romero 

Create the vhost file for Apache 

    sudo vi /etc/apache2/sites-available/jose_romero.jobsitychallenge.conf

and place the following content on it

    <VirtualHost *:80>
        ServerName jose_romero.jobsitychallenge.com
        DocumentRoot /var/www/jobsitychallenge/jose_romero/public
    	ErrorLog ${APACHE_LOG_DIR}/error.log
     	CustomLog ${APACHE_LOG_DIR}/access.log combined
    </VirtualHost>

Add the new site to the enabled sites list

    sudo a2ensite jose_romero.jobsitychallenge.conf

Restart Apache web server to take effect the changes.

    sudo systemctl restart apache2

Create database on MySQL 

    jobsity-joseromero

Adjust .env file located on root folder of project for DB server settings

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=jobsity-joseromero
    DB_USERNAME=root
    DB_PASSWORD=

After downloading, run the following set of commands on root folder of the project

    composer install
    npm install && npm run dev
    php artisan migrate
    php artisan db:seed

At this point, everything must be ready for testing. Click on the following link to begin

[http://jose_romero.jobsitychallenge.com](http://jose_romero.jobsitychallenge.com/)

There are 2 default users created

       username = test1@domain.com
       password = testpassword1
    
       username = test2@domain.com
       password = testpassword2

For testing, you must enable sqlite module/libraary on php.ini for proper testing runing

       ./vendor/bin/phpunit

on project root folder

