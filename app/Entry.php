<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    protected $fillable = [
        'title', 'content', 'user_id'
    ];

    protected $casts = [
        'user_id' => 'integer'
    ];


    public function user(){
        return $this->belongsTo('App\User');

    }

}
