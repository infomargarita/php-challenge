<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response;
use App\Entry;
use App\User;
use App\Tweet;
use DB;
//use ReliqArts\Thujohn;
use ReliqArts\Thujohn\Twitter\Facades\Twitter;



class EntriesController extends Controller
{
    /**
     * Check for user auth
     *
     * @return void
     */

    public function AuthCheck() {
        if (!Auth::check()) {
            abort(403, 'Unauthorized action');
        }
    }

    /**
     * Check entry ownership
     *
     * @return void
     */
    public function OwnerCheck($entry){

        if ($entry->user_id !== Auth::user()->id) {
            abort(403, 'You are not allowed to edit or delete this entry');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function hideOrShowTweet(Request $request) {

        $id = $request->input('id');

        $tweet = Tweet::FirstOrNew(['tweet_id' => $id]);

        if ($tweet->exists) {
            $tweet->hidden = (!$tweet->hidden);
        }
        else {
          $tweet->tweet_id = $id;
          $tweet->hidden = true;
          $tweet->user_id = Auth::user()->id;
        }
        //dd($tweet);
        $tweet->save();
        return Response::json($tweet->hidden);
    }

    public function index($user = null)
    {
        if ($user == null)  {
          if (Auth::check()) {
              $user = Auth::user()->id;
          }
          else {
              abort(403, 'Unauthorized action');
          }
        }

        $user = User::FindOrFail($user);

        $entries = Entry::when($user, function ($q) use ($user) {
            return $q->where('user_id', $user->id);
        })->with('user')->orderby('created_at', 'desc')->paginate(5);

        // get tweets from user time line
        $tweets = collect(json_decode(Twitter::getUserTimeline(['screen_name' => $user->twitter_handle, 'count' => 5, 'format' => 'json'])));
        // get hiodden tweets for current logged in user
        $hiddenTweets = $user->hiddenTweets()->get();

        // iterate over timeline to check if we nee dto hide some tweet
        $tweets = $tweets->map(function ($t) use ($hiddenTweets) {
            $t->hidden  = $hiddenTweets->contains('tweet_id', $t->id_str);
            return $t;
        });


      return view('entries-index', compact('entries', 'user', 'tweets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->AuthCheck();
        $entry = new Entry;
        return view('entries-edit', compact('entry'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $e = new Entry;
        $e->title =  $request->input('title');
        $e->content = $request->input('content');
        $e->user_id = Auth::user()->id;
        $e->save();

        Session::flash('success', 'Entries created');

        return redirect()->route('entries.index');

//        return response()->view('index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entry = Entry::FindOrFail($id);
        //dd($entry);
        return view('entries-show', compact('entry'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->AuthCheck();

        $entry = Entry::FindOrFail($id);

        $this->OwnerCheck($entry);


        return view('entries-edit', compact('entry'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->AuthCheck();

        $entry = Entry::FindOrFail($id);


        $this->OwnerCheck($entry);

        $entry->title = $request->input('title');
        $entry->content = $request->input('content');

        $entry->save();



        return redirect()->route('entries.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->AuthCheck();

        $entry = Entry::FindOrFail($id);

        $this->OwnerCheck($entry);

        $entry->delete();

        return Response::json(true);
    }
}
