<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entry;
use App\User;


class IndexController extends Controller
{

    public function index()
    {
        //
        // At fiurst, I'm getting last 3 entries for EACH user
        //

        // $entries = collect([]);
        // $users = User::has('entries')->get();
        // foreach($users as $u) {
        //   $e = Entry::with('user')->where('user_id', $u->id)->orderby('created_at', 'desc')->limit(3)->get();
        //   foreach($e as $entry)
        //     $entries->push($entry);
        // }


        // but reading againg, I realize that were the last 3 entries for ALL users, so is much more simple

        $entries = Entry::with('user')->orderby('created_at', 'desc')->limit(3)->get()->paginate(5);

        return view('index', compact('entries'));
    }
}
