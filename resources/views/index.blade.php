@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @empty($entries)
              <h1>No entries to display</h1>
            @else
              <h3>Showing latest 3 entries of ALL users </h3>
              <br>
              @each('entry-detail-partial', $entries, 'entry')
              </br>
              {{$entries->links()}}
              {{--  <a href="/entries">Back to entries list</a>  --}}
            @endempty

        </div>
    </div>
</div>
@endsection
@section('document-ready')
    @include('entry-delete-js-partial')
@endsection
</script>
