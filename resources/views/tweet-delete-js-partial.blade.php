    // function to handle tweet hidding/unhidding button click
    $(".btnHideTweet").unbind('click').bind('click', function (e) {
        e.preventDefault();
        // get  element clicked data
        var elem = $(this);
        var id = elem.data('id');
        //run ajax request
        $.ajax({
            url: '/entries/hideOrShowTweet',
            type: 'POST',
            data: {id:  id},
            cache: false,
            success: function (hidden) {
                // Change element data accordding tweet status returned
                if (hidden) {
                    elem.toggleClass("btn-success");
                    elem.toggleClass("btn-danger");
                    elem.html('Show tweet');
                }
                else {
                    elem.toggleClass("btn-success");
                    elem.toggleClass("btn-danger");
                    elem.html('Hide tweet');
                }
                return true;
            },
            error: function (data) {
                console.log('Error: '+data)
                return false;
            }
        });
    });
