    // function to handle entry deletion confirmation and execution
    $(".btnDeleteEntry").unbind('click').bind('click', function (e) {
        e.preventDefault();
        if (!confirm('Are you sure to delete this entry?')) {
            return false;
        }
        // get  element clicked data
        var elem = $(this);
        var id = elem.data('id');
        //run ajax request
        $.ajax({
            url: '/entries/'+id,
            type: 'DELETE',
            //data: {id:  id},
            cache: false,
            success: function (data) {
                // reload page
                location.reload();
                return true;
            },
            error: function (data) {
                console.log('Error: '+data)
                return false;
            }
        });
    });
