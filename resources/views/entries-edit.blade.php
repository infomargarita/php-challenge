@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <form class="form-horizontal" action="{{$entry->exists ? route('entries.update', $entry->id) : route('entries.store') }}"
                method="POST">
                <fieldset>
                    @if($entry->exists)
                        <legend>Edit entry</legend>
                        <input type="hidden" name="_method" value="PATCH">
                    @else
                    <legend>Create entry</legend>
                    @endif
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="textinput">Title</label>
                      <div class="col-md-8">
                        <input id="title" name="title" type="text"  class="form-control input-md" required="" value="{{$entry->title}}">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-4 control-label" for="content">Content</label>
                      <div class="col-md-12">
                        <textarea required="" class="form-control" id="content" name="content" rows="10" cols="50">{{$entry->content}}</textarea>
                      </div>
                    </div>
                    {{ csrf_field() }}
                    <button class="btn btn-success" type="submit">Save</button>
                </fieldset>
                </form>
        </div>
    </div>
</div>
@endsection
