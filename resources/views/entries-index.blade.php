@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @isset($user)
                <div> <h2>Entries for user {{$user->name}} </h2></div>
            @endif
            @empty($entries)
                <h1>No entries to display</h1>
            @else
                <br>
                @each('entry-detail-partial', $entries, 'entry')
            @endempty
            {{$entries->links()}}
            <a href="/">Back to home</a>
        </div>

        <div class="col-xs-4 col-sm-4 :col-md-4 col-lg-4">
            <div><h3>Latest tweets</h3></div>
            <div id="tweets">
                  @empty($tweets)
                    <h1>No tweets to display</h1>
                  @else
                    <p>Showing latest  tweets for {{$user->twitter_handle}} twitter account</p>
                    <br>
                    @foreach($tweets as $tweet)
                        {{--  if tweet owner is logged user, allow to hide.show tweets options  --}}
                        @if(Auth::check() && Auth::user()->id == $user->id)
                            <div class="card" style="width: 18rem;">
                                <div class="card-body">
                                <p class="card-text">{{$tweet->text}}</p>
                                    @if($tweet->hidden)
                                    <a id="btnShowHide{{$tweet->id_str}}" class="btnHideTweet btn btn-secondary btn-danger" data-id="{{$tweet->id_str}}"  href="#">Show tweet</a>
                                    @else
                                    <a id="btnShowHide{{$tweet->id_str}}" class="btnHideTweet btn btn-secondary btn-success" data-id="{{$tweet->id_str}}" href="#">Hide tweet</a>
                                    @endif
                                </div>
                            </div>
                            </br>
                        @else
                          {{--  else, check if tweet was not set hidden by its owner  --}}
                          @if(!$tweet->hidden)
                            <div class="card" style="width: 18rem;">
                                <div class="card-body">
                                <p class="card-text">{{$tweet->text}}</p>
                                </div>
                            </div>
                            </br>
                          @endif
                        @endif

                    @endforeach
                  @endempty
            </div>
        </div>

    </div>
</div>
@endsection
@section('document-ready')
    @include('entry-delete-js-partial')
    @include('tweet-delete-js-partial')
@endsection
