<div>
<h2><a href="/entries/{{$entry->id}}/">{{$entry->title}}</a></h2>
@if(Auth::check())
  @if(Auth::user()->id == $entry->user_id)
    <div class="row">
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
            <a class="btn btn-small  btn-success" href="/entries/{{$entry->id}}/edit" role="button">Edit</a>
        </div>
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
            <a class="btnDeleteEntry btn btn-small btn-danger" href="#" data-id="{{$entry->id}}" role="button">Delete</a>
        </div>
    </div>
  @endif
@endif

<small>
    {{$entry->created_at}} by <a href="/entries/user/{{$entry->user_id}}">{{$entry->user->name}}</a>
</small>


{{--  <p>
{{$entry->content}}
</p>  --}}

</div>
</br>
