@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2><a href="/entries/{{$entry->id}}/show">{{$entry->title}}</a></h2>
            @if(Auth::check())
                @if(Auth::user()->id == $entry->user_id)
                <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <a class="btn btn-small  btn-success" href="/entries/{{$entry->id}}/edit" role="button">Edit</a>
                    </div>
                </div>
                @endif
            @endif

            <small>
                {{$entry->created_at}} by <a href="/entries/{{$entry->user_id}}">{{$entry->user->name}}</a>
            </small>
            <p>
                {{htmlspecialchars(nl2br($entry->content))}}
            </p>
        </div>
    </div>
</div>
@endsection
