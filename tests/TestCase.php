<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\User;
use App\Entry;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;


    // return a created user from the factory

    public function getUser($attr = []){
        return factory(User::class)->create($attr);
    }

    public function getEntry($attr = []){
        return factory(Entry::class)->create($attr);
    }

    public function login($user, $pass = 'thepassword' ){
        return $this->from('/login')->post('/login', [
            'email' => $user->email,
            'password' => $pass,
        ]);

    }
}
