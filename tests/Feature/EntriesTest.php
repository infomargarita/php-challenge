<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use App\Entries;

class EntriesTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_non_authenticated_user_can_not_create_entries()
    {
        $response = $this->get('/entries/create');
        $response->assertStatus(403);
    }

    public function test_authenticated_user_can_not_edit_other_users_entries()
    {
        // cretae an entry that belongs to user1
        $user1 = $this->getUser();
        $entry = $this->getEntry(['user_id' => $user1->id]);

        $user2 = $this->getUser();

        $response = $this->login($user2);
        $response = $this->get("/entries/{$entry->id}/edit");
        $response->assertStatus(403);
    }

    public function test_authenticated_user_can_not_delete_other_users_entries()
    {
        $user1 = $this->getUser();
        $entry = $this->getEntry(['user_id' => $user1->id]);

        $user2 = $this->getUser();

        $response = $this->login($user2);
        $response = $this->call('DELETE', "/entries/{$entry->id}");
        $response->assertStatus(403);
    }

    public function test_authenticated_user_can_edit_entries()
    {
        $user = $this->getUser();
        $entry = $this->getEntry(['user_id' => $user->id]);
        $response = $this->login($user);
        $response = $this->actingAs($user)->get("/entries/{$entry->id}/edit");
        $response->assertStatus(200);
    }

    public function test_authenticated_user_can_delete_entries()
    {
        $user = $this->getUser();
        $entry = $this->getEntry(['user_id' => $user->id]);
        $response = $this->login($user);
        $response = $this->call('DELETE', "/entries/{$entry->id}");
        $response->assertStatus(200);
        $this->assertDatabaseMissing('entries', ['id'=> $entry->id]);
    }


}
