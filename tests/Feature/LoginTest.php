<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;

class TestLogin extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_login_page_can_be_shown()
    {
        $response = $this->get('/login');
        $response->assertSuccessful();
        $response->assertViewIs('auth.login');
    }

    public function test_authenticated_user_can_not_view_a_login_form()
    {
        $user =  $this->getUser();

        $response = $this->actingAs($user)->get('/login');
        $response->assertRedirect('/home');
    }

    public function test_user_with_correct_credentials_can_login()
    {
        $user = $this->getUser();

        $response = $this->login($user);

        $response->assertRedirect('/entries');
        $this->assertAuthenticatedAs($user);
    }

    public function test_user_with_incorrect_password_can_not_login()
    {
        $user = $this->getUser(['password' => bcrypt($password = 'thepassword')]);

        $response = $this->login($user, 'badpassword');

        // $response = $this->from('/login')->post('/login', [
        //     'email' => $user->email,
        //     'password' => 'bad_password',
        // ]);

        $response->assertRedirect('/login');
        $response->assertSessionHasErrors('email');

        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));
        $this->assertGuest();
    }

}
